using System;
using System.Security.Cryptography;
using Alarm;
using ClockApp;

namespace Tp4
{

    class Main_program
    {
        internal static void Program()
        {
            int choice;
             Console.WriteLine("");
             Console.WriteLine("""
            *********************************************
            *********************************************
            ******************** TP N°4 *****************
            *******************  HORLOGE *****************
            *********************************************
            *********************************************
            """);

            do
            {
                DisplayMenu();
                choice = GetChoice();
                Console.Clear();

                switch (choice)
                {
                    case 1:
                        AlarmApp alarmApp = new AlarmApp();
                        alarmApp.Run();
                        break;
                    case 2:
                        ClockApp.ClockApp.RunClockApp();
                        break;

                    default:
                        break;
                }
            } while (choice != 3);

            void DisplayMenu()

            {
           
            Console.WriteLine("");
            Console.WriteLine("""
            #############################################
            ################### MENU ####################
            #############################################
            ################# 1) Alarme #################
            ################# 2) Horloge ################
            ################# 3) Quitter ################
            #############################################
            ########## NB:: Entrer des integers #########
            """);
            Console.Write(":::: > Votre choix : ");
            
            }
            
            int GetChoice()
            {
                int choice;
                while (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine(" ===== Entrer invalide =======");
                    Console.Write(" :::: > Votre choix : ");
                }
                return choice;
            }



        }
    }
}