using System;
using Cercle_class;
using Point_class;

namespace Tp2
{


    class Main_program
    {


        internal static void Program()

        {
            Console.WriteLine("");
             Console.WriteLine("""
            *********************************************
            *********************************************
            ******************** TP N°2 *****************
            *******************  CERCLE *****************
            *********************************************
            *********************************************
            """);
            Console.Write(" ::::> Donner l'abscisse du centre : ");
            double x = double.Parse(Console.ReadLine());
            Console.Write(" ::::> Donner l'ordonné du centre : ");
            double y = double.Parse(Console.ReadLine());
            Console.Write(" ::::> Donner le rayon : ");
            double r = double.Parse(Console.ReadLine());

            //création d'un cercle
            Console.WriteLine("************ CERCLE **********");
            Cercle cercle = new Cercle(x, y, r);
            cercle.Display();
            cercle.Perimetre();
            cercle.Surface();

            //input point à verifier
            Console.WriteLine(" ::::> Donner un point à verifier ::::<  ");
            Console.Write("  ::::> Son X : ");
            int monx = int.Parse(Console.ReadLine() ?? "");
            Console.Write("  ::::> Son Y : ");
            int mony = int.Parse(Console.ReadLine() ?? "");

            //creation d'un point
            Point point = new Point(monx, mony);
            point.View();

            //verification de l'appartenance du point 
            point.VerificationPoint(monx, mony, x, y, r);

        }
    }
}