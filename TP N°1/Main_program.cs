using System;
using Client_class;
using Compte_class;
namespace Tp1
{


    class Main_program
    {


        internal static void Program()
        {
#pragma warning disable CS8632
            string? cin, firstName, lastName, tel;
#pragma warning restore CS8632
            float? somme;
            Console.WriteLine("""
            *********************************************
            *********************************************
            ******************** TP N°1 *****************
            **************** COMPTE BANQUE **************
            *********************************************
            *********************************************
            """);

            // #Create Compte Client 1
         
            Console.WriteLine("####### COMPTE 1 #########");
            Console.Write($"Donner Le CIN: ");
            cin = Console.ReadLine();
            Console.Write($"Donner Le Nom: ");
            firstName = Console.ReadLine();
            Console.Write($"Donner Le Prénom: ");
            lastName = Console.ReadLine();
            Console.Write($"Donner Le numéro de télephone: ");
            tel = Console.ReadLine();

            Compte cpt1 = new Compte(new Client(cin, firstName, lastName, tel));
            Console.WriteLine($"Détails du compte:{cpt1.Resumer()}");


            // # Crediter & Debiter Compte Client 1

            // Crediter cpt 1
            Console.Write($"Donner le montant à déposer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Crediter((float)somme);
            Console.WriteLine($" == Opération bien effectuée{cpt1.Resumer()} ==");
            //Debiter cpt 1
            Console.Write($"Donner le montant à retirer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Debiter((float)somme);
            Console.Write($" == Opération bien effectuée{cpt1.Resumer()} == ");
            
            Console.ReadKey();
            // # Create Compte Client 2
            Console.WriteLine("####### COMPTE 2 #########");
            Console.Write($"\n\n\nCompte 2:\nDonner Le CIN: ");
            cin = Console.ReadLine();
            Console.Write($"Donner Le Nom: ");
            firstName = Console.ReadLine();
            Console.Write($"Donner Le Prénom: ");
            lastName = Console.ReadLine();
            Console.Write($"Donner Le numéro de télephone: ");
            tel = Console.ReadLine();

            Compte cpt2 = new Compte(new Client(cin, firstName, lastName, tel));
            Console.WriteLine($"Détails du compte:{cpt2.Resumer()}");


            Console.ReadKey();
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //Crediter cpt2 -> cpt1
            Console.WriteLine($":: Crediter le compte {cpt2.Code} à partir du compte {cpt1.Code} :: ");
            Console.Write($"Donner le montant à déposer: ");
            somme = float.Parse(Console.ReadLine());
            cpt2.Crediter((float)somme, cpt1);
            Console.WriteLine($" == Opération bien effectuée == ");
             Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            
            
            //Debiter cpt1 -> cpt2
            Console.WriteLine($"Débiter le compte {cpt1.Code} et créditer le compte {cpt2.Code}");
            Console.Write($"Donner le montant à retirer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Debiter((float)somme, cpt2);
            Console.WriteLine($"== Opération bien effectuée == ");

             
              Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
               Console.WriteLine("############# RESUMER  ##############");
            Console.WriteLine($"{cpt1.Resumer()}{cpt2.Resumer()}");
            Console.Write($"\n\n\n{Compte.NombreCompteCree()}");
            Console.ReadKey();
        }
    }
}