using System;
using Client_class;

namespace Compte_class{
    class Compte
    {
        private float solde;
        public float Solde { get { return solde; } }
        private uint code;
        public uint Code { get { return code; } }
        public Client Proprietaire { get; set; }

        private static uint compteur = 0;

        public string Resumer()
        {
            return $@"
        ************************
        Numéro de Compte: {Code}
        Solde de compte: {Solde}
        Propriétaire du compte:
        {Proprietaire.Afficher()}
        
        ************************";
        }

        public static string NombreCompteCree()
        {
            return $"Le nombre de comptes crées: {compteur}";
        }

        public void Debiter(float montant, Compte compteClient)
        {
            Debiter(montant);
            compteClient.Crediter(montant);
        }

        public void Debiter(float montant)
        {
            solde -= montant;
        }

        public void Crediter(float montant, Compte compteClient)
        {
            Crediter(montant);
            compteClient.Debiter(montant);
        }

        public void Crediter(float montant)
        {
            solde += montant;
        }

        public Compte(Client client, float montant)
        {
            Proprietaire = client;
            solde = montant;
            code = ++compteur;
        }

        public Compte(Client client)
        {
            Proprietaire = client;
            solde = default;
            code = ++compteur;
        }
    }
}