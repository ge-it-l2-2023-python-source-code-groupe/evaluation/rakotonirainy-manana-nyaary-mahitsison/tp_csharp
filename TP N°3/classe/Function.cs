using System;
using System.Security.Cryptography;
using MyPlayer;
using Historique;
using System.Collections.Generic;

namespace Function
{
    public class Newgame


    {
        static List<GameRound> roundsHistory = new();
        static readonly Dictionary<string, int> playersScores = new();
        public static void NewPlayer()
        {
            Console.Write("\nEntrer le nombre de joueur : ");
            int size = Int32.Parse(Console.ReadLine() ?? "");

            string namePlayer = "";


            for (int i = 1; i <= size; i++)
            {
                Console.Write($"\nEntrer le nom du joueur {i} : ");
                string name = Console.ReadLine() ?? "";

                namePlayer += name + ",";

                Player player = new Player(name);
                playersScores[name] = player.Score;




            }

            //Affichage des noms 

            string[] namePlayerfull = namePlayer.Split(',');

            Console.WriteLine("\n>>>>>> Les noms des joeurs sont : <<<<<<<<<\n");

            foreach (var name in namePlayerfull)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    Console.WriteLine($"----> {name} >>>>>>  Score =0\n");

                }


            }


            //Appelle de la function Manche 
            Manche(namePlayerfull);

        }


        //Function Manche
        static void Manche(string[] players)
        {
            GameRound currentRound = new GameRound();
            Console.WriteLine("\n");
            Console.WriteLine(
            """
            ***********************************
            *************   MANCHE   **********
            ***********************************
            """);
            foreach (var player in players)
            {
                if (!string.IsNullOrEmpty(player))
                {
                    Console.WriteLine($" \n>>>>>>  Tour de {player} ");
                    Console.WriteLine("======> Voulez vous lancer le dé y(yes) or n(no)  ");
                    Console.Write("======> ");
                    string  choix = Console.ReadLine();
                    switch (choix)
                    {
                        case "y":
                            Run_dé(player, currentRound);
                            break;
                        case "n":
                            Console.WriteLine("------ Vous avez passer votre tour ------\n");
                            break;
                        default:
                            break;
                    }

                }

            }

            roundsHistory.Add(currentRound);

            Console.WriteLine("-----------  SCORE FINAL  ----------");

            foreach (var player in players)
            {
                if (!string.IsNullOrEmpty(player))
                {
                    // Vérifiez si le joueur a un score enregistré
                    if (playersScores.ContainsKey(player))
                    {
                        // Affichez le nom du joueur et son score
                        Console.WriteLine($" >>>>>> {player} == {playersScores[player]}");
                    }
                    else
                    {
                        // Si le joueur n'a pas de score enregistré, affichez un message approprié
                        Console.WriteLine($" >>>>>> {player}== 0 ");
                    }
                }
            }
        }



        //function lancement de dé 
        static void Run_dé(string currentPlayer, GameRound currentRound)
        {
            Random random = new Random();
            int randomNumber = random.Next(1, 7);

            if (randomNumber == 1)
            {
                Console.WriteLine("|     |\n|  *  |\n|     |");

            }
            if (randomNumber == 2)
            {
                Console.WriteLine("|*    |\n|     |\n|    *|");

            }
            if (randomNumber == 3)
            {
                Console.WriteLine("|*    |\n|  *  |\n|    *|");

            }
            if (randomNumber == 4)
            {
                Console.WriteLine("|*   *|\n|     |\n|*   *|");

            }
            if (randomNumber == 5)
            {
                Console.WriteLine("|*   *|\n|  *  |\n|*   *|");

            }
            if (randomNumber == 6)
            {
                Console.WriteLine("|* * *|\n|* * *|\n|* * *|");
                playersScores[currentPlayer] += 2;
                currentRound.PointsEarned[currentPlayer] = 2;
                currentRound.PlayerScores[currentPlayer] = playersScores[currentPlayer];



            }

        }
        static string AfficherHistoriqueManches(List<GameRound> rounds)
        {
            string historique = "";

            foreach (var round in rounds)
            {
                historique += $" Date et heure de la manche : {round.RoundDateTime}\n";
                foreach (var kvp in round.PlayerScores)
                {
                    historique += $"{kvp.Key} - Score : {kvp.Value}";

                    // Vérifie si les points gagnés sont définis pour ce joueur dans cette manche
                    if (round.PointsEarned.ContainsKey(kvp.Key))
                    {
                        historique += $" - Points gagnés : {round.PointsEarned[kvp.Key] - 1}\n";
                    }
                    else
                    {
                        historique += " - Points gagnés : Non enregistré\n";
                    }
                }
                historique += "--------------------------------------\n";
            }

            return historique;
        }

        public static void History()
        {
            string affichage = AfficherHistoriqueManches(roundsHistory);
            Console.WriteLine(affichage);


        }




    }
}