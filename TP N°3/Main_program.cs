using System;

using Function;



namespace Tp3
{
    class Main_program

    {

        internal static void Program()
        {
            Console.WriteLine("");
            Console.WriteLine("""
            *********************************************
            *********************************************
            ******************** TP N°3 *****************
            ****************** JEU DE DÉ ****************
            *********************************************
            *********************************************
            """);

            bool program = true;
            while (program != false)
            {
                Console.WriteLine(
                   """
       ###################################################
       ########## 1) Nouvelle manche 
       ########## 2) Voir l'historique des manches 
       ########## 3) Quitter le programme 
       ###################################################
       
       """
                   );
                Console.Write("Votre choix  \n  -------> : ");
                int userChoice = Int32.Parse(Console.ReadLine() ?? "");

                switch (userChoice)
                {
                    case 1:
                        Console.WriteLine("******************Nouvelle manche****************");
                        Function.Newgame.NewPlayer();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.WriteLine("******************Historique des manches************");
                        Console.WriteLine("***JUSTE LES SCORES DES JOEURS GAGNANT QUI SERONT AFFICHER***");
                        Function.Newgame.History();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("*************** --------->Vous quittez le programme----------<************");
                        Console.ReadKey();
                        Console.Clear();
                        program = false;

                        break;
                    default:
                        Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>  Il y une erreur ");
                        break;
                }
            }



        }

    }
}






















